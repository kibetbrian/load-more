<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $primaryKey = 'payId';
    public $timestamps = false;

   /**
    * @var array
    */
   protected $fillable = ['client_id', 'leaseId', 'unitId','unitCode','thirdpartyPaymentRef', 'hasRefund', 'paidInBy', 'paidInByMobile', 'paymentDate', 'amountDue', 'amountPaid', 'penaltyFee', 'paymentFor', 'paymentType', 'isRent', 'rentMonth', 'rentYear', 'notes', 'lastUpdated', 'ipAddress', 'source', 'status','thirdpartyTranRef'];
}
