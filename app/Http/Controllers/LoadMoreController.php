<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoadMoreController extends Controller
{
    function index()
    {
     return view('load_more');
    }

    function load_data(Request $request)
    {
     if($request->ajax())
     {
      if($request->id > 0)
      {
       $data = DB::table('payments')
          ->where('payId', '<', $request->id)
          ->orderBy('payId', 'DESC')
          ->limit(7)
          ->get();
      }
      else
      {
       $data = DB::table('payments')
          ->orderBy('payId', 'DESC')
          ->limit(7)
          ->get();
      }
      $output = '';
      $last_id = '';
      $output .= '
      <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                  <tr>
                      <th>Id</th>
                      <th>Amount</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Date</th>
                  </tr>
              </thead>
              <tbody>
              ';
      if(!$data->isEmpty())
      {

       foreach($data as $row)
       {
        $output .= '
                        <tr>
                            <th>' .$row->payId.'</th>
                            <th>' .$row->paymentDate. '</th>
                            <th>' .$row->paymentDate. '</th>
                            <th>' .$row->paymentDate. '</th>
                            <th>' .$row->paymentDate. '</th>
                        </tr>
                   ';

        $last_id = $row->payId;
       }
       $output .= '
                        </tbody>
                    </table>
                    </div>
                </div>';
       $output .= '
       <div id="load_more">
       <div class="text-center">
        <button type="button" name="load_more_button" class="btn btn-success" data-id="'.$last_id.'" id="load_more_button">Load More</button>
        </div>
       </div>
       ';
      }
      else
      {
       $output .= '
       <div id="load_more">
        <button type="button" name="load_more_button" class="btn btn-info form-control">No Data Found</button>
       </div>
       ';
      }
      echo $output;
     }
    }
}


